using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Api.Controllers;

namespace Bw.Api.Controllers
{

    /// <summary>
    /// BeerCategory Api Controller
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    [RoutePrefix("api/BeerCategory")]
    public partial class BeerCategoryController : BaseApiController
    {
        #region fields
        private readonly IBeerCategoryService beercategoryService;
        #endregion

        #region constructors

        public BeerCategoryController(IBeerCategoryService beercategoryService)
        {
            this.beercategoryService = beercategoryService;
        }

        #endregion


        #region public actions

        [HttpPost]        
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] BeerCategoryCriteria criteria)
        {
            int totalRecords = 0;
            var result = beercategoryService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetBeerCategoryById/{id}", Name = "GetBeerCategoryById")]
        public HttpResponseMessage GetBeerCategoryById([FromUri] Guid id)
        {
            var data = beercategoryService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllBeerCategory")]
        public HttpResponseMessage GetAllBeerCategory()
        {
            var data = beercategoryService.GetAll(AreaId);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] BeerCategoryViewModel viewModel)
        {
            //viewModel.CreatedBy = User.Identity.Name;
            viewModel.CreatedDate = DateTime.Now;
		     var result = beercategoryService.Add(viewModel);
             return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


		[HttpPut]
        public HttpResponseMessage Update(BeerCategoryViewModel viewModel)
        {
            viewModel.UpdatedBy = User.Identity.Name;
            viewModel.UpdatedDate = DateTime.Now;
            var result = beercategoryService.Update(viewModel);            
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = beercategoryService.Remove(id);
			return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
