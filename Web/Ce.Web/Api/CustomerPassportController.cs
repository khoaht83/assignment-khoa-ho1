using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Api.Controllers;

namespace Bw.Api.Controllers
{

    /// <summary>
    /// CustomerPassport Api Controller
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    [RoutePrefix("api/CustomerPassport")]
    public partial class CustomerPassportController : BaseApiController
    {
        #region fields
        private readonly ICustomerPassportService customerpassportService;
        #endregion

        #region constructors

        public CustomerPassportController(ICustomerPassportService customerpassportService)
        {
            this.customerpassportService = customerpassportService;
        }

        #endregion


        #region public actions

        [HttpPost]        
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] CustomerPassportCriteria criteria)
        {
            int totalRecords = 0;
            var result = customerpassportService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetCustomerPassportById/{id}", Name = "GetCustomerPassportById")]
        public HttpResponseMessage GetCustomerPassportById([FromUri] Guid id)
        {
            var data = customerpassportService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllCustomerPassport")]
        public HttpResponseMessage GetAllCustomerPassport()
        {
            var data = customerpassportService.GetAll(AreaId);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] CustomerPassportViewModel viewModel)
        {

		     var result = customerpassportService.Add(viewModel);
             return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


		[HttpPut]
        public HttpResponseMessage Update(CustomerPassportViewModel viewModel)
        {

            var result = customerpassportService.Update(viewModel);            
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = customerpassportService.Remove(id);
			return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
