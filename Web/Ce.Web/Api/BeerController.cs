using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Api.Controllers;

namespace Bw.Api.Controllers
{

    /// <summary>
    /// Beer Api Controller
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    [RoutePrefix("api/Beer")]
    public partial class BeerController : BaseApiController
    {
        #region fields
        private readonly IBeerService beerService;
        #endregion

        #region constructors

        public BeerController(IBeerService beerService)
        {
            this.beerService = beerService;
        }

        #endregion


        #region public actions

        [HttpPost]        
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] BeerCriteria criteria)
        {
            int totalRecords = 0;			
            var result = beerService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetBeerById/{id}", Name = "GetBeerById")]
        public HttpResponseMessage GetBeerById([FromUri] Guid id)
        {

            var data = beerService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllBeer")]
        public HttpResponseMessage GetAllBeer()
        {
            var data = beerService.GetAll(AreaId);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] BeerViewModel viewModel)
        {
            //viewModel.CreatedBy = User.Identity.Name;
            viewModel.CreatedDate = DateTime.Now;
		     var result = beerService.Add(viewModel);
             return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


		[HttpPut]
        public HttpResponseMessage Update(BeerViewModel viewModel)
        {
            viewModel.UpdatedBy = User.Identity.Name;
            viewModel.UpdatedDate = DateTime.Now;
            var result = beerService.Update(viewModel);            
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = beerService.Remove(id);
			return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
