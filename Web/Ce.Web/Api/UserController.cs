using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;

namespace Ce.Api.Controllers
{

    /// <summary>
    /// User Api Controller
    /// </summary>
    [RoutePrefix("api/User")]
    public partial class UserController : BaseApiController
    {
        #region fields
        private readonly IUserService userService;
        #endregion

        #region constructors

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        #endregion


        #region public actions

        [HttpPost]        
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] UserCriteria criteria)
        {
            int totalRecords = 0;
            var result = userService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetUserById/{id}", Name = "GetUserById")]
        public HttpResponseMessage GetUserById([FromUri] Guid id)
        {
            var data = userService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllUser")]
        public HttpResponseMessage GetAllUser()
        {
            var data = userService.GetAll(AreaId);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] UserViewModel viewModel)
        {

		     var result = userService.Add(viewModel);
             return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


		[HttpPut]
        public HttpResponseMessage Update(UserViewModel viewModel)
        {

            var result = userService.Update(viewModel);            
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = userService.Remove(id);
			return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
