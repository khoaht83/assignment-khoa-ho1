using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Api.Controllers;

namespace Bw.Api.Controllers
{

    /// <summary>
    /// Manufacturer Api Controller
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    [RoutePrefix("api/Manufacturer")]
    public partial class ManufacturerController : BaseApiController
    {
        #region fields
        private readonly IManufacturerService manufacturerService;
        #endregion

        #region constructors

        public ManufacturerController(IManufacturerService manufacturerService)
        {
            this.manufacturerService = manufacturerService;
        }

        #endregion


        #region public actions

        [HttpPost]        
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] ManufacturerCriteria criteria)
        {
            int totalRecords = 0;
			var result = manufacturerService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetManufacturerById/{id}", Name = "GetManufacturerById")]
        public HttpResponseMessage GetManufacturerById([FromUri] Guid id)
        {
            var data = manufacturerService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllManufacturer")]
        public HttpResponseMessage GetAllManufacturer()
        {
            var data = manufacturerService.GetAll(AreaId);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] ManufacturerViewModel viewModel)
        {
            //viewModel.CreatedBy = User.Identity.Name;
            viewModel.CreatedDate = DateTime.Now;
		     var result = manufacturerService.Add(viewModel);
             return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


		[HttpPut]
        public HttpResponseMessage Update(ManufacturerViewModel viewModel)
        {
            viewModel.UpdatedBy = User.Identity.Name;
            viewModel.UpdatedDate = DateTime.Now;
            var result = manufacturerService.Update(viewModel);            
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = manufacturerService.Remove(id);
			return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
