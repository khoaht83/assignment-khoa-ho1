using System;


namespace Ce.Api.Controllers
{
    using System.Collections.Generic;
    using System.Web.Http;
    public partial class BaseApiController : ApiController
    {
        public Guid AreaId { get; set; }
    }

    public partial class BaseController : System.Web.Mvc.Controller
    {
        public Guid AreaId { get; set; }

    }
}

namespace Ce.Web.Controllers
{
    using System.IO;
    using System.Web.Mvc;
    public partial class BaseController : Controller
    {

        public Guid AreaId { get; set; }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }


        protected string SaveFileToDisk(byte[] bytes, string fileName)
        {
            string url = string.Empty;

            try
            {
                string folderName = string.Format("{0:yyyyMMdd}", DateTime.Now);
                string xsltFolder = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(@"~/uploads"), folderName);

                if (!Directory.Exists(xsltFolder))
                {
                    Directory.CreateDirectory(xsltFolder);
                }

                string xsltPath = Path.Combine(xsltFolder, fileName);

                System.IO.FileStream _fileStream = new System.IO.FileStream(xsltPath, System.IO.FileMode.Create,
                                      System.IO.FileAccess.Write);

                _fileStream.Write(bytes, 0, bytes.Length);

                // close file stream
                _fileStream.Close();

                url = string.Format("{0}://{1}/{2}/{3}/{4}", Request.Url.Scheme, Request.Url.Authority, "uploads", folderName, fileName);
            }
            catch (Exception ex)
            {

            }

            return url;
        }

    }


}