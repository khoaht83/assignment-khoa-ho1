﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Ce.Web.Startup))]
namespace Ce.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {            
            ConfigureAuth(app);
            app.MapSignalR();
            //ConfigureOAuth(app); 
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            //app.CreatePerOwinContext(UserDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            
            //use a cookie to temporarily store information about a user logging in with a thir
        }
    }
}
