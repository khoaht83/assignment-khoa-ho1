'use strict';

define(['app'], function (app) {

    var injectParams = ['$q', '$parse', '$filter'];

    var autoComplete = function ($q, $parse, $filter) {
        return {
            scope: {
                suggestions: '=data',
                Id: '=ngModel'
            },
            template: '<div class="autoComplete">'
                         + '<input required type="text" ng-model="textTyping" id="textTyping" class="form-control" maxlength="255" />'
                         + '<ul ng-show="isShowSuggestion">'
                             + '<li ng-mouseenter="suggestionOnHover($index)" ng-click="selectSuggestion(item)" ng-repeat="item in suggestionsFiltered" ng-class="{autoCompleteLiActive: $index==currentIndex}" index="{{$index}}">{{item.Name}}</li>'
                         + '</ul>'
                      + '</div>',
            controller: function ($scope) {

                $scope.Id = "";
                $scope.currentIndex = -1;
                $scope.isShowSuggestion = false;

                $scope.selectSuggestion = function (suggestion) {
                    $scope.textTyping = suggestion.Name;
                    $scope.Id = suggestion.Id;
                }

                this.setTextTyping = function (suggestionId, suggestions) {

                    if (!suggestionId) {
                        $scope.textTyping = "";
                    }

                    if (suggestions) {
                        $scope.suggestionsFiltered = suggestions;
                    }

                    if (suggestionId && suggestions.length > 0)
                        angular.forEach($scope.suggestions, function (item) {
                            if (item.Id == suggestionId) {
                                $scope.textTyping = item.Name;
                            }
                        });
                }

                $scope.suggestionOnHover = function (index) {
                    $scope.currentIndex = index;
                }

            },
            restrict: "E",
            require: ["^form", "^autoComplete"],
            link: function (scope, element, attrs, ctrlArray) {

                var parentFormController = ctrlArray[0];
                var autoCompleteController = ctrlArray[1];

                var key = { left: 37, up: 38, right: 39, down: 40, enter: 13, esc: 27 };

                var autoCompleteTextbox = element.find('#textTyping');
                autoCompleteTextbox.on('focus', function () {
                    scope.isShowSuggestion = true;
                    scope.$apply();
                });
                autoCompleteTextbox.on('blur', function () {
                    setTimeout(function () {
                        scope.isShowSuggestion = false;
                        if (scope.textTyping == "") {
                            scope.Id = null;
                        }
                        scope.$apply();
                    }, 200);

                });

                autoCompleteTextbox.on('keydown', function (e) {
                    var keycode = e.keyCode || e.which;
                    switch (keycode) {
                        case key.down:
                            if (scope.currentIndex < (scope.suggestionsFiltered.length - 1)) {
                                scope.currentIndex++;
                            }
                            break;
                        case key.up:
                            if (scope.currentIndex > 0) {
                                scope.currentIndex--;
                            }
                            break;
                        case key.enter:
                            e.preventDefault();
                            scope.selectSuggestion(scope.suggestionsFiltered[scope.currentIndex]);
                            scope.isShowSuggestion = false;
                            break;
                        case key.esc:
                            scope.isShowSuggestion = false;
                            break;
                        default:
                            scope.isShowSuggestion = true;
                            scope.currentIndex = -1;
                            break;
                    }
                    scope.$apply();
                });

                scope.$watch('textTyping', function (e) {
                    scope.suggestionsFiltered = $filter("autoCompleteFilter")(scope.suggestions, scope.textTyping);
                })

                scope.$watch("Id", function (newValue, oldValue) {
                    if (oldValue != "" && (oldValue != newValue)) {
                        parentFormController.$dirty = true;
                    }
                    autoCompleteController.setTextTyping(scope.Id, scope.suggestions);
                });
                scope.$watch("suggestions", function () {
                    autoCompleteController.setTextTyping(scope.Id, scope.suggestions);
                });
            }
        }
    };

    autoComplete.$inject = injectParams;

    app.directive('autoComplete', autoComplete);

});