﻿'use strict';

define(['app'], function (app) {

    var injectParams = [];

    var dropMultiple = function () {
        return {
            link: function (scope, element, attrs) {
                element.multiselect({
                    buttonClass: 'btn',
                    buttonWidth: 'auto',
                    buttonContainer: '<div class="btn-group" />',
                    maxHeight: false,
                    buttonText: function (options) {

                        if (options.length == 0) {
                            return 'Chọn ngày';
                        }
                        else if (options.length > 3) {
                            return options.length + ' được chọn';
                        }
                        else {
                            var selected = '';
                            options.each(function () {
                                selected += $(this).text() + ', ';
                            });
                            return selected.substr(0, selected.length - 2);
                        }


                    }
                });

                // Watch for any changes to the length of our select element
                scope.$watch(function () {
                    return element[0].length;
                }, function () {
                    element.multiselect('rebuild');
                });

                // Watch for any changes from outside the directive and refresh
                scope.$watch(attrs.ngModel, function () {
                    element.multiselect('refresh');
                });

            }
        }
    };

    dropMultiple.$inject = injectParams;

    app.directive('dropMultiple', dropMultiple);
});

