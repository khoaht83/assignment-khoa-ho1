'use strict';

define(['app'], function (app) {

    var injectParams = [];

    var formatDate = function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attr, modelCtrl) {
                modelCtrl.$formatters.push(function (modelValue) {

                    if (modelValue != undefined) {
                        //debugger
                        var strDate = parseInt(modelValue.replace(/(^.*\()|([+-].*$)/g, ''))

                        modelValue = new Date(strDate);
                        return modelValue;

                    }

                })
            }
        }
    };

    formatDate.$inject = injectParams;

    app.directive('formatDate', formatDate);


    var dateFormat = function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                var format = attr.dateFormat;
                //Angular 1.3 insert a formater that force to set model to date object, otherwise throw exception.
                //Reset default angular formatters/parsers
                ngModelCtrl.$formatters.length = 0;
                ngModelCtrl.$parsers.length = 0;


                ngModelCtrl.$formatters.push(function (valueFromModel) {
                    //return how data will be shown in input
                    if (valueFromModel) {
                        return moment(valueFromModel).format(format);
                    }
                    else
                        return null;
                });
                ngModelCtrl.$parsers.push(function (valueFromInput) {
                    if (valueFromInput) {

                        return moment(valueFromInput, format).toDate();
                    }
                    else
                        return null;
                });
            }
        };
    };

    app.directive('dateFormat', formatDate);
});
