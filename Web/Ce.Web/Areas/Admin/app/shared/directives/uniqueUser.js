﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['usersService'];

    var checkunique = function (userService) {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {

                var cachedValue = "";
                var isFirstWatch = true;

                scope.$watch('form.$valid', function (validity) {
                    scope.modalInvalid = !validity;
                })

                scope.$watch(function () {
                    if (isFirstWatch) {
                        cachedValue = elem.val();
                        isFirstWatch = false;
                    }
                    return elem.val();
                }, function (value) {
                });

                elem.on('blur', function (event) {
                    scope.$apply(function () {
                        var value = elem.val();
                        var typeForCheckUnique = attrs.name;
                        if (typeForCheckUnique == 'UserName') {
                            var viewModel = {
                                UserName: value
                            };

                            userService.checkUser(viewModel).then
                            (function (response) {
                                ctrl.$setValidity('uniqueuser', (!response.data) || (cachedValue == value));
                                scope.modalInvalid = scope.itemForm.$invalid;
                            }, scope.processError);
                        }
                        else if (typeForCheckUnique == 'Email') {
                            var viewModel = {
                                Email: value
                            };

                            userService.checkUser(viewModel).then
                            (function (response) {
                                ctrl.$setValidity('uniqueuser', (!response.data) || (cachedValue == value));
                                scope.modalInvalid = scope.itemForm.$invalid;
                            }, scope.processError);
                        }
                    });
                });
            }
        };
    }
    checkunique.$inject = injectParams;

    app.directive('uniqueuser', checkunique);
});
