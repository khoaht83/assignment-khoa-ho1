'use strict';

define(['app'], function (app) {

    var injectParams = ['$q', '$parse', '$filter'];

    var compareTo = function ($q, $parse, $filter) {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    };

    compareTo.$inject = injectParams;

    app.directive('compareTo', compareTo);

});