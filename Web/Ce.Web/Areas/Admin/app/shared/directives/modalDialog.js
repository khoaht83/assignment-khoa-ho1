﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$q'];

    var modalDialog = function ($q) {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            template:
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<h4 ng-bind="dialogTitle"></h4>' +
                    '</div>' +
                    '<div class="modal-body"></div>' +
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-default" ng-click="cancel()">Đóng</button>' +
                        '<button type="button" class="btn btn-primary" ng-click="ok()">Save</button>' +
                    '</div>' +
                '</div>',
            link: function (scope, element, attrs, controller, transclude) {

                transclude(scope, function (clone) {
                    var modalBody = element[0].querySelector('.modal-body');
                    angular.element(modalBody).append(clone);
                });

                scope.$watch(attrs.visible, function (value) {
                    if (value == true)
                        $(element).modal('show');
                    else
                        $(element).modal('hide');
                });

                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });

            }
        };
    };

    modalDialog.$inject = injectParams;

    app.directive('modalDialog', modalDialog);

});