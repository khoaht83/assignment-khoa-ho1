'use strict';

define(['app'], function (app) {


    var injectParams = ['$http', '$q'];

    var AreasFactory = function ($http, $q) {
        var serviceBase = '/api/area';
        var factory = {};

        factory.create = function (obj) {
            return $http.post(serviceBase + "/Create", obj).then(function (results) {
                obj.Id = results.Id;
                return results.Data;
            });
        }

        factory.update = function (obj) {
            return $http.post(serviceBase + "/Update", obj).then(function (results) {
                obj.Id = results.Id;
                return results.data;
            });
        }


        factory.delete = function (areaId) {
            return $http.delete(serviceBase + '/Delete/' + areaId).then(function (status) {
                return status;
            });
        }

        factory.getById = function (id) {
            return $http.get(serviceBase + '/GetById/' + id).then(function (results) {
                return results.data;
            });
        }

        factory.getAll = function () {
            return $http.get(serviceBase + '/GetAll').then(function (results) {
                return results;
            });
        }


        factory.search = function (areaCriteria) {

            return $http.post(serviceBase + "/Search", areaCriteria).then(function (response) {
                return response;
            });
        };


        return factory;

    }

    AreasFactory.$inject = injectParams;
    app.factory('areasService', AreasFactory);
});

