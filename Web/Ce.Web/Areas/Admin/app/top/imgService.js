﻿
'use strict';

define([], function () {

    var appSetting = angular.module('lv.media', []);

    var injectParams = ['$scope', '$http', '$filter', '$location', '$timeout', '$uibModalInstance', '$uibModal'];

    var UploadImageController = function ($scope, $http, $filter, $location,
        $timeout, $uibModalInstance, $uibModal) {

        $scope.errorMessage = "";
        $scope.showToolsbox = false;
        $scope.UrlImg = null;
        $scope.isErrorLimit = false;
        $scope.isErrorSize = false;
        var $image = $('#cropper > img');

        function beforeSend() {
            $('#btnCropImage').button('loading');
            $('.product_image-modal_footer-btn-text1').attr('disabled', 'disabled');
            $('.product_image-modal_footer-btn-text2').attr('disabled', 'disabled');
        }
        function afterSend() {
            $('#btnCropImage').button('reset');
            $('.product_image-modal_footer-btn-text1').removeAttr('disabled');
            $('.product_image-modal_footer-btn-text2').removeAttr('disabled');
        }
        function showMessage(data) {
            //console.log(data);
        }
        function processError(error) {
            showMessage({
                message: error.data.Message,
                type: 'danger'
            });
        }

        //------------------Crop Image ------------------------//
        $scope.uploadImageWithCropping = function (e) {
            $scope.isErrorSize = false;
            $scope.isErrorLimit = false;
            var file = new Image();
            var limitSize = $scope.LimitSize;
            file = document.getElementById("uploadImageWithCropping").files[0];
            if (file) {
                var sizeLimit = 1024 * 1024 * limitSize; //max size of a photo = 4 MB
                var fileSize = file.size;
                if (fileSize > sizeLimit) {
                    $scope.isErrorSize = true;
                    return false;
                } else {
                    //Display uploaded photo file from stream
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        //Set file path to image-cropper
                        $image.cropper("destroy");
                        $('#image-cropper').attr('src', reader.result);
                        $scope.showToolsbox = true;
                        $scope.$apply();
                        $image = $('#cropper > img');
                        $image.cropper("setAspectRatio", 0 / 0);
                        setTimeout(function () {
                            $image.cropper("clear");
                        }, 100);
                    };
                    reader.readAsDataURL(file);

                    return true;
                };
            } else {
                return false;
            }
        };
        //Draw canvas that it's content is rotated image
        $scope.rotateAndDrawCanvas = function (image, degrees) {
            var canvas = document.createElement("canvas");
            var ctx = canvas.getContext("2d");

            if (degrees == 90 || degrees == 270) {
                canvas.width = image.height;
                canvas.height = image.width;
            } else {
                canvas.width = image.width;
                canvas.height = image.height;
            }

            //Set background color = white
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = 'white';
            ctx.fillRect(0, 0, canvas.width, canvas.height);

            //Draw rotated image into canvas
            if (degrees == 90 || degrees == 270) {
                ctx.translate(image.height / 2, image.width / 2);
            } else {
                ctx.translate(image.width / 2, image.height / 2);
            }
            ctx.rotate(degrees * Math.PI / 180);
            ctx.drawImage(image, -image.width / 2, -image.height / 2);

            return canvas.toDataURL();
        };

        //Rotate the origin image
        $scope.rotateBase64Image = function (base64ImageSrc, rotateDegree) {
            //Create temp image
            var image = document.createElement("img");

            //Set image's data
            image.src = base64ImageSrc;

            //Return canvas
            if (image.complete) {
                return $scope.rotateAndDrawCanvas(image, rotateDegree);
            } else {
                image.onload = function () {
                    return $scope.rotateAndDrawCanvas(image, rotateDegree);
                }
            }
        };

        //Button "Close" is clicked
        $scope.closeModalCropImage = function () {
            //Toogle on/off the modal of cropper
            $('#cropper-modal').modal('toggle');
            //Reset cropper plugin
            $image.cropper("reset");
        };

        //Enable crop function
        $scope.enableCrop = function () {
            $image.cropper("crop");
        }

        //Button Default is clicked - Disable crop function and revert rotation
        $scope.disableCrop = function () {
            //Revert rotation
            var rotateDegree = $image.cropper('getData').rotate;
            $scope.rotateImage(-rotateDegree);
            //Disable crop
            $image.cropper("clear");

        }

        //Button "3:2" / "2:3" / "1:1" is clicked
        $scope.changeCropRatio = function (ratio) {
            $image = $('#cropper > img');
            $scope.disableCrop();
            $scope.enableCrop();
            $image.cropper("setAspectRatio", ratio);
        }

        //Button "rotate-left" / "rotate-right" is clicked
        $scope.rotateImage = function (degree) {
            $image.cropper("rotate", degree);
        }

        //Save image
        $scope.saveCroppedImage = function () {
            
            var file = document.getElementById("uploadImageWithCropping").files[0];
            var croppedImage;

            //Check image is cropped or not
            if ($image.cropper('getCroppedCanvas').localName === "canvas") {
                //Get cropped part of image
                croppedImage = $image.cropper('getCroppedCanvas').toDataURL();
            }
            else {
                //Get full image that is not cropped
                croppedImage = $('#cropper > img')[0].src;

                //If client rotate the image, rotate and draw new canvas
                var rotateDegree = $image.cropper('getData').rotate;
                if (rotateDegree != 0) {
                    croppedImage = $scope.rotateBase64Image(croppedImage, rotateDegree);
                }
            }



            beforeSend();


            var data = { ImgData: croppedImage };
            $http.post('api/GoogleDrive/UploadImage', data).then(function (results) {
                $uibModalInstance.close(results.data);
                return results.data;
            });

        };
        //--------------------- End Crop Image ------------------------//
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    };

    UploadImageController.$inject = injectParams;
    var mediaUploadService = function ($uibModal) {
        var factory = {};
        factory.open = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/app/shared/partials/uploadImage.html',
                controller: UploadImageController,
                size: 'lg',
                backdrop: 'static',
                keyboard: false
            });

            return modalInstance.result.then(function (result) {
                return result;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };
        return factory;
    }
    //mediaUploadService.$inject = ['$scope', '$uibModal'];
    appSetting.factory('mediaUploadService', mediaUploadService);
});