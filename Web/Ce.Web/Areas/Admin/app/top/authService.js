define([], function () {

    var injectParams = ['$http', '$q', 'localStorageService', 'ngSetting'];

    var authFactory = function ($http, $q, localStorageService, ngSetting) {

        var serviceBase = ngSetting.apiServiceBaseUri;
        var roleServiceBase = ngSetting.apiServiceBaseUri + 'api/user/get';
        var authServiceFactory = {};

        var _authentication = {
            isAuth: false,
            userName: "",
            id: null,
            fullName: "",
            useRefreshTokens: false,
            EcenterRole: {}
        };

        var _externalAuthData = {
            provider: "",
            userName: "",
            externalAccessToken: ""
        };

        var _saveRegistration = function (registration) {

            _logOut();

            return $http.post(serviceBase + 'api/User/register', registration).then(function (response) {
                return response;
            });

        };

        var _login = function (loginData) {

            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

            if (loginData.useRefreshTokens) {
                data = data + "&client_id=" + ngSetting.clientId;
            }

            var deferred = $q.defer();

            $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                var req = { role: response.role };
                $.ajax({
                    url: serviceBase + 'api/User/GetEcenterPermissions?role=' + response.role,
                    method: 'GET'
                }).done(function (data) {

                    if (loginData.useRefreshTokens) {
                        localStorageService.set('authorizationData',
                            {
                                token: response.access_token,
                                role: response.role,
                                isAdmin: response.role == "Administrator",
                                userName: loginData.userName,
                                id: response.id,
                                lastName: response.lastName,
                                imgProfile: response.imgProfile,
                                refreshToken: response.refresh_token,
                                useRefreshTokens: true,
                                EcenterRole: data
                            });
                    }
                    else {
                        localStorageService.set('authorizationData',
                            {
                                token: response.access_token,
                                role: response.role,
                                userName: loginData.userName,
                                id: response.id,
                                lastName: response.lastName,
                                imgProfile: response.imgProfile,
                                refreshToken: "",
                                useRefreshTokens: false,
                                EcenterRole: data,
                                isAdmin: response.role == "Administrator"
                            });
                    }
                    _authentication.isAuth = true;
                    _authentication.userName = loginData.userName;
                    _authentication.role = response.role;
                    _authentication.id = response.id;
                    _authentication.lastName = response.lastName;
                    _authentication.imgProfile = response.imgProfile;
                    _authentication.EcenterRole = data;
                    _authentication.useRefreshTokens = loginData.useRefreshTokens;
                    _authentication.isAdmin = response.role == "Administrator",
                    deferred.resolve(response);

                });



            }).error(function (err, status) {
                _logOut();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        var _logOut = function () {

            localStorageService.remove('authorizationData');
            _authentication.isAuth = false;
            _authentication.userName = "";
            _authentication.lastName = "";
            _authentication.role = "";
            _authentication.id = null;
            _authentication.useRefreshTokens = false;
            _authentication.EcenterRole = null;
            _authentication.isAdmin = null;

        };

        var _fillAuthData = function () {

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
                _authentication.lastName = authData.lastName;
                _authentication.role = authData.role;
                _authentication.id = authData.id;
                _authentication.imgProfile = authData.imgProfile;
                _authentication.useRefreshTokens = authData.useRefreshTokens;
                _authentication.EcenterRole = authData.EcenterRole;
                _authentication.isAdmin = authData.isAdmin
            }
        };

        var _refreshToken = function () {
            var deferred = $q.defer();

            var authData = localStorageService.get('authorizationData');

            if (authData) {

                if (authData.useRefreshTokens) {

                    var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + ngSetting.clientId;

                    localStorageService.remove('authorizationData');

                    $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                        localStorageService.set('authorizationData',
                            {
                                token: response.access_token,
                                userName: response.userName,
                                refreshToken: response.refresh_token,
                                useRefreshTokens: true
                            });

                        deferred.resolve(response);

                    }).error(function (err, status) {
                        _logOut();
                        deferred.reject(err);
                    });
                }
            }

            return deferred.promise;
        };

        var _obtainAccessToken = function (externalData) {

            var deferred = $q.defer();

            $http.get(serviceBase + 'api/User/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {

                localStorageService.set('authorizationData',
                    {
                        token: response.access_token,
                        userName: response.userName,
                        refreshToken: "",
                        useRefreshTokens: false
                    });

                _authentication.isAuth = true;
                _authentication.userName = response.userName;
                _authentication.lastName = response.lastName;
                _authentication.role = response.role;
                _authentication.id = response.id;
                _authentication.useRefreshTokens = false;
                _authentication.isAdmin = response.role == "Administrator";
                deferred.resolve(response);

            }).error(function (err, status) {
                _logOut();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        var _registerExternal = function (registerExternalData) {

            var deferred = $q.defer();

            $http.post(serviceBase + 'api/User/registerexternal', registerExternalData).success(function (response) {

                localStorageService.set('authorizationData',
                    {
                        token: response.access_token,
                        userName: response.userName,
                        refreshToken: "",
                        useRefreshTokens: false
                    });

                _authentication.isAuth = true;
                _authentication.userName = response.userName;
                _authentication.lastName = response.lastName;
                _authentication.role = response.role;
                _authentication.id = response.id;
                _authentication.useRefreshTokens = false;
                _authentication.isAdmin = response.role == "Administrator";
                deferred.resolve(response);

            }).error(function (err, status) {
                _logOut();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        authServiceFactory.saveRegistration = _saveRegistration;
        authServiceFactory.login = _login;
        authServiceFactory.logOut = _logOut;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.authentication = _authentication;
        authServiceFactory.refreshToken = _refreshToken;

        authServiceFactory.obtainAccessToken = _obtainAccessToken;
        authServiceFactory.externalAuthData = _externalAuthData;
        authServiceFactory.registerExternal = _registerExternal;

        return authServiceFactory;

    };
    authFactory.$inject = injectParams;

    var authInterceptorServiceInjectParams = ['$q', '$injector', '$location', 'localStorageService'];
    var authInterceptorService = function ($q, $injector, $location, localStorageService) {
        var vm = this;

        var authInterceptorServiceFactory = {};

        var _request = function (config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        var _responseError = function (rejection) {


            if (rejection.status === 401) {
                var authService = $injector.get('authService');
                var authData = localStorageService.get('authorizationData');

                if (authData) {
                    if (authData.useRefreshTokens) {
                        $location.path('/refresh');
                        return $q.reject(rejection);
                    }
                }
                authService.logOut();
                window.location.href = "/Home#/login";
            }
            return $q.reject(rejection);
        }

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;

        return authInterceptorServiceFactory;
    };
    authInterceptorService.$inject = authInterceptorServiceInjectParams;


    var appSetting = angular.module('lv.authentication', [])
    appSetting.factory('authInterceptorService', authInterceptorService);
    appSetting.factory('authService', authFactory);


});
