﻿
'use strict';

define([], function () {

    var serviceBase = 'http://localhost:28570/';
    //var serviceBase = 'http://logviet.com/';

    var appSetting = angular.module('lv.Settings', [])

    var injectParams = ['$uibModal'];
    //var injectParams = [];

    var modalService = function ($uibModal) {


        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: '/app/shared/partials/modalConfirm.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Thay ddo',
            bodyText: 'Perform this action?'
        };



        this.openModal = function (modalPath, customModalOptions) {

            var modal = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: modalPath
            };

            return this.show(modal, customModalOptions);
        };


        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };



        this.alertInfor = function (message) {
            var customModalOptions = {
                templateUrl: '/app/shared/partials/modalInfo.html',
                controller: function ($scope, $uibModalInstance) {
                    $scope.alertText = message;
                    $scope.closeModal = function () {
                        $uibModalInstance.close('dismiss');
                    }
                }
            };
            $uibModal.open(customModalOptions);
        }

        this.alertError = function (message) {
            var customModalOptions = {
                actionButtonText: 'Đồng ý',
                bodyText: message
            };
            var customModalDefaults = {
                templateUrl: '/app/shared/partials/modalError.html'
            }
            this.showModal(customModalDefaults, customModalOptions);
        }

        this.showMessageConfirm = function (message, okFunction, cancelFuntion) {

            var modalOptions = {
                closeButtonText: 'Close',
                actionButtonText: 'Đồng ý',
                headerText: 'Xác nhận',
                bodyText: message
            };

            this.showModal({}, modalOptions).then(function (result) {
                if (result === 'ok') {
                    if (okFunction) {
                        okFunction();
                    }

                }
                else {
                    if (cancelFuntion) {
                        cancelFuntion();
                    }
                }
            });
        }

        this.show = function (customModalDefaults, customModalOptions) {


            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in this service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in this service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $uibModalInstance) {


                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $uibModalInstance.close('ok');
                    };
                    $scope.modalOptions.close = function (result) {
                        $uibModalInstance.close('cancel');
                    };
                };

                tempModalDefaults.controller.$inject = ['$scope', '$uibModalInstance'];
            }

            return $uibModal.open(tempModalDefaults).result;
        };

        this.showPopup = function (controller, view, width, height, data, callBackFunction) {
            var customModalOptions = {
                backdrop: 'static',
                animation: true,
                templateUrl: view,
                controller: controller,
                scope: data,
                resolve: {

                }
            };

            var modalInstance = $uibModal.open(customModalOptions);

            //modalInstance.opened.then(function () {
            //    debugger;
            //    if ($(".modal-dialog").length > 0) {
            //        debugger;
            //    }
            //});

            modalInstance.result.then(function (data) {
                callBackFunction(data);
            }, function () {
            });

            setTimeout(function () {
                if (width != undefined && width != null) {
                    $(".modal-dialog").css("width", width);
                }

            }, 500);

        }

    };

    modalService.$inject = injectParams;

    appSetting.service('modalService', modalService);

    appSetting.constant('ngSetting', {
        apiServiceBaseUri: serviceBase,
        clientId: 'lvApp'
    });

    appSetting.constant('ENUMS',
        {
            ClassStatus: { 0: 'Đang dự kiến mở', 1: 'Đang nghi danh', 2: 'Tạm hoãn', 3: 'Bi hủy', 4: 'Đang học', 5: 'Đã hoàn thành' },
            TeacherTypes: { 0: 'Hợp đồng', 1: 'Thỉnh giảng', 2: 'Học việc', 3: 'Cơ hữu' }
            , LocationList: [{ Id: 0, Name: 'Top' }, { Id: 1, Name: 'Left' }, { Id: 2, Name: 'Right' }, { Id: 3, Name: 'Bottom' }]
            , LocationArray: { 0: 'Top', 1: 'Right', 2: 'Left', 3: 'Bottom' }
            , ArticleStatusArray: { 0: 'Nháp', 1: 'Chưa được đăng', 2: 'Đã được đăng' }
            , ArticleStatus: [{ Id: 0, Name: 'Nháp' }, { Id: 1, Name: 'Chưa được đăng' }, { Id: 2, Name: 'Đã được đăng' }]
            , DayOfWeeks: { 0: 'Chủ nhật', 1: 'Thứ 2', 2: 'Thứ 3', 3: 'Thứ 4', 4: 'Thứ 5', 5: 'Thứ 6', 6: 'Thứ 7' }
            , RegisterStatusArray: { 0: 'Đăng ký new', 1: 'Duyệt', 2: 'Không duyệt', 3: 'Close' }
            , RegisterStatus: [{ Id: 0, Name: 'Đăng ký new' }, { Id: 1, Name: 'Duyệt' }, { Id: 2, Name: 'Close' }, { Id: 3, Name: 'Close' }]
        });
});