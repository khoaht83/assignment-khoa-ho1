'use strict';

define(['app'], function (app) {
    app.controller('HomeController', ['$scope', '$location', '$stateParams', '$timeout', function ($scope, $location, $stateParams, $timeout) {
        var vm = this;
        var box = null;
        var userName = 'admin';
        var email = "ngoaingubroadway@gmail.com";

        $(document).ready(function () {

            // Declare a proxy to reference the hub.
            //var chatHub = $.connection.chatHub;

            //registerClientMethods(chatHub);

            //// Start Hub
            //$.connection.hub.start().done(function () {

            //    console.info('chathub client started!');
            //    chatHub.server.connect(email, userName, 132564);

            //});




            //registerClientMethods(chatHub);

        });



        function registerClientMethods(chatHub) {

            chatHub.client.onConnected = function (id, userName, allUsers, messages) {

                $('#hdId').val(id);
                $('#hdUserName').val(userName);
                $('#spanUser').html(userName);

            }

            chatHub.client.messageReceived = function (userName, message) {
                var received = { userName: userName, message: message };
                console.log(received);
                $("#chat_div").chatbox("option", "boxManager").addMsg(userName, message);
            }

            chatHub.client.sendPrivateMessage = function (windowId, fromEmail, message) {



                if (!box) {

                    box = $("#chat_div").chatbox(
                    {
                        id: windowId,
                        user: userName,
                        focus: true,
                        title: fromEmail,
                        messageSent: function (id, user, msg) {
                            chatHub.server.sendPrivateMessage(fromEmail, msg);
                        }
                    });


                }

                $("#chat_div").chatbox("option", "boxManager").addMsg(fromEmail, message);
            }

        }

    }]);

});

