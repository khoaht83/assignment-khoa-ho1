'use strict';

define(['app'], function (app) {

app.controller('UserRolesController', ['$scope', '$filter', '$location', 'NgTableParams',
                    '$timeout', 'userRolesService', 'modalService',function ($scope, $filter, $location, NgTableParams,
    $timeout, userRolesService, modalService) {

    var vm = this;
    vm.userRoles = [];
    vm.currentPage = 1;
    vm.itemPerPage = 10;
    vm.totalRecords = 0;
    vm.errorMessage = "";
    vm.sortDirection = "asc";
    vm.columnName = "name";

    vm.searchText = "";

	initalData();

    function processError(error) {
        vm.errorMessage = error.message;
        App.alert({
            message: error.data.Message,
            type: 'danger'
        });
    };


    vm.searchTextChanged = function () {
        doSearch();
    };


    function initalData() {
		vm.tableParams = new NgTableParams({
				page: 1,
				count: 10,
				filter: {
					ModelNum: ''
				},
				sorting:
				{
					ModelNum: 'asc'
				}
			},
			{
				getData: function ($defer, params) {
					var criteria = {
						CurrentPage: params.page() - 1,
						ItemPerPage: params.count(),
						SortColumn: vm.columnName,
						SortDirection: vm.sortDirection,
						Name: vm.searchText
					};

					userRolesService.search(criteria).then(function (response) {

						vm.userRoles = response.data.Data;
						var orderedData = params.sorting() ?
							$filter('orderBy')(vm.userRoles, params.orderBy())
											: vm.userRoles;
						params.total(response.data.TotalRecords);
						$defer.resolve(orderedData);

					});


				}
			}, function (error) {
				console.log('errror', error);
			});
    }

	$scope.$on('doSearch', function (event, areaId) {
		initalData();
    });

    vm.edit = function (id) {
        $location.path('/userrole/' + id);
    };

    vm.delete = function (id) {

        var delObj = getUserRoleById(id);
        var objName = delObj.Name == undefined ? 'userRole' : delObj.Name;
		modalService.showMessageConfirm("Do you want to delete " + objName + " ?", function () {
                userRolesService.delete(id).then(function () {
                    for (var i = 0; i < vm.userRoles.length; i++) {
                        if (vm.userRoles[i].Id === id) {
                            vm.userRoles.splice(i, 1);
							vm.tableParams.reload();
                            break;
                        }
                    }

                }, function (error) {
                    App.alert({
                        message: error.data.Message,
                        type: 'danger'
                    });
                });
        });
    };

    function getUserRoleById(id) {
        for (var i = 0; i < vm.userRoles.length; i++) {
            var delObj = vm.userRoles[i];
            if (delObj.Id === id) {
                return delObj;
            }
        }
        return null;
    }    

    function doSearch() {

        vm.tableParams.reload();
    }

}]);

});

