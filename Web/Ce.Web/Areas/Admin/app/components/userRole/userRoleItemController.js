'use strict';

define(['app'], function (app) {
	app.controller('UserRoleItemController', ['$scope', '$location', '$stateParams'/*[@DependenciesService1]*/

						, '$timeout', 'userRolesService', 'modalService',function ($scope, $location, $stateParams/*[@DependenciesService2]*/						

						, $timeout, userRolesService, modalService) {

		var vm = this,
			userRoleId = ($stateParams.userRoleId) ? $stateParams.userRoleId : '',
			timer,
			onRouteChangeOff;

		vm.userRole = {};
		// for userRole Only		
		vm.formTitle = userRoleId == 0 ? 'Create userRole new' : 'Update userRole';		
		vm.isRepeat = true;

		/*get[@GetParentTables]*/	


		vm.saveUserRole = function () {
			if ($scope.itemForm.$valid) {
				if (!vm.userRole.Id) {
					userRolesService.create(vm.userRole).then(processSuccess, processError);
				}
				else {
					userRolesService.update(vm.userRole).then(processSuccess, processError);
				}
				$location.path('/userRoles');
			}
		};

		vm.delete = function (userRoleId) {
			var headerText = 'userRole';
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete UserRole',
				headerText: 'Delete ' + headerText + '?',
				bodyText: 'Do you want to delete userRole?'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					userRolesService.delete(userRoleId).then(function () {
						onRouteChangeOff(); //Stop listening for location changes
						$location.path('/userRoles');
					}, processError);
				}
			});
		};

		function init() {

			if (userRoleId != '0') {
				userRolesService.get(userRoleId)ById.then(function (data) {
					vm.userRole = angular.copy(data);
				}, processError);
			}

			/*get[@CallGetParentTables]*/


			//Make sure they're warned if they made a change but didn't save it
			//Call to $on returns a "deregistration" function that can be called to
			//remove the listener (see routeChange() for an example of using it)
			onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

		}

		init();

		function routeChange(event, newUrl, oldUrl) {
			//Navigate to newUrl if the form isn't dirty
			if (!vm.itemForm || !vm.itemForm.$dirty) return;

			var modalOptions = {
				closeButtonText: 'Close',
				actionButtonText: 'Cancel',
				headerText: 'Data has changed!',
				bodyText: 'The data has changed, do you want to exist??'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					onRouteChangeOff(); //Stop listening for location changes
					$location.path($location.url(newUrl).hash()); //Go to page they're interested in
				}
			});

			//prevent navigation by default since we'll handle it
			//once the user selects a dialog option
			event.preventDefault();
			return;
		}

		function processSuccess() {
			ShowMessage({
				message: 'Success.'
			});
			if (vm.isRepeat) {
				vm.userRoleId = 0;
				vm.userRole = null;
				ClearTextFields();
			}
			else
			{
				$location.path('/userRoles');
			}
		}

		function processError(error) {
			ShowMessage({
				message: error.data.Message,
				type: 'danger'
			});
		}
	}]);

});

