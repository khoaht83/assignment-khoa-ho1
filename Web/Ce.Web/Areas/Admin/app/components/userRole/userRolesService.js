'use strict';

define(['app'], function (app) {

	var injectParams = ['$http', '$q'];

	var UserRolesFactory = function ($http, $q) {
		var serviceBase = 'api/userrole';
		var factory = {};

		factory.create = function (obj) {
			return $http.post(serviceBase + "/Create", obj).then(function (results) {
				obj.Id = results.Id;
				return results.Data;
			});
		}

		factory.update = function (obj) {
			return $http.put(serviceBase + "/Update", obj).then(function (results) {
				obj.Id = results.Id;
				return results.data;
			});
		}


		factory.delete = function (userroleId) {
			return $http.delete(serviceBase + '/Delete/'+userroleId).then(function (status) {
				return status;
			});
		}

		factory.getById = function (id) {
			return $http.get(serviceBase + '/GetByUserRoleId/' + id).then(function (results) {
				return results.data;
			});
		}

		factory.getAll = function () {
			return $http.get(serviceBase + '/GetAllUserRole').then(function (results) {
				return results;
			});
		}


		factory.search = function (userroleCriteria) {

			return $http.post(serviceBase + "/Search", userroleCriteria).then(function (response) {
				return response;
			});
		};


		return factory;

	}

	UserRolesFactory.$inject = injectParams;
	app.factory('userRolesService', UserRolesFactory);

});
