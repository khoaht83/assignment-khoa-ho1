'use strict';

define(['app'], function (app) {

	var injectParams = ['$http', '$q'];

	var CustomerPassportsFactory = function ($http, $q) {
		var serviceBase = 'api/customerpassport';
		var factory = {};

		factory.create = function (obj) {
			return $http.post(serviceBase + "/Create", obj).then(function (results) {
				obj.Id = results.Id;
				return results.Data;
			});
		}

		factory.update = function (obj) {
			return $http.put(serviceBase + "/Update", obj).then(function (results) {
				obj.Id = results.Id;
				return results.data;
			});
		}


		factory.delete = function (customerpassportId) {
			return $http.delete(serviceBase + '/Delete/'+customerpassportId).then(function (status) {
				return status;
			});
		}

		factory.getById = function (id) {
			return $http.get(serviceBase + '/GetCustomerPassportById/' + id).then(function (results) {
				return results.data;
			});
		}

		factory.getAll = function () {
			return $http.get(serviceBase + '/GetAllCustomerPassport').then(function (results) {
				return results;
			});
		}


		factory.search = function (customerpassportCriteria) {

			return $http.post(serviceBase + "/Search", customerpassportCriteria).then(function (response) {
				return response;
			});
		};


		return factory;

	}

	CustomerPassportsFactory.$inject = injectParams;
	app.factory('customerPassportsService', CustomerPassportsFactory);

});
