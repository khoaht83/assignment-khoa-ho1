'use strict';

define(['app'], function (app) {

app.controller('CustomerPassportsController', ['$scope', '$filter', '$location', 'NgTableParams',
                    '$timeout', 'customerPassportsService', 'modalService',function ($scope, $filter, $location, NgTableParams,
    $timeout, customerPassportsService, modalService) {

    var vm = this;
    vm.customerPassports = [];
    vm.currentPage = 1;
    vm.itemPerPage = 10;
    vm.totalRecords = 0;
    vm.errorMessage = "";
    vm.sortDirection = "asc";
    vm.columnName = "name";

    vm.searchText = "";

	initalData();

    function processError(error) {
        vm.errorMessage = error.message;
        App.alert({
            message: error.data.Message,
            type: 'danger'
        });
    };


    vm.searchTextChanged = function () {
        doSearch();
    };


    function initalData() {
		vm.tableParams = new NgTableParams({
				page: 1,
				count: 10,
				filter: {
					ModelNum: ''
				},
				sorting:
				{
					ModelNum: 'asc'
				}
			},
			{
				getData: function ($defer, params) {
					var criteria = {
						CurrentPage: params.page() - 1,
						ItemPerPage: params.count(),
						SortColumn: vm.columnName,
						SortDirection: vm.sortDirection,
						Name: vm.searchText
					};

					customerPassportsService.search(criteria).then(function (response) {

						vm.customerPassports = response.data.Data;
						var orderedData = params.sorting() ?
							$filter('orderBy')(vm.customerPassports, params.orderBy())
											: vm.customerPassports;
						params.total(response.data.TotalRecords);
						$defer.resolve(orderedData);

					});


				}
			}, function (error) {
				console.log('errror', error);
			});
    }

	$scope.$on('doSearch', function (event, areaId) {
		initalData();
    });

    vm.edit = function (id) {
        $location.path('/customerpassport/' + id);
    };

    vm.delete = function (id) {

        var delObj = getCustomerPassportById(id);
        var objName = delObj.Name == undefined ? 'customerPassport' : delObj.Name;
		modalService.showMessageConfirm("Do you want to delete " + objName + " ?", function () {
                customerPassportsService.delete(id).then(function () {
                    for (var i = 0; i < vm.customerPassports.length; i++) {
                        if (vm.customerPassports[i].Id === id) {
                            vm.customerPassports.splice(i, 1);
							vm.tableParams.reload();
                            break;
                        }
                    }

                }, function (error) {
                    App.alert({
                        message: error.data.Message,
                        type: 'danger'
                    });
                });
        });
    };

    function getCustomerPassportById(id) {
        for (var i = 0; i < vm.customerPassports.length; i++) {
            var delObj = vm.customerPassports[i];
            if (delObj.Id === id) {
                return delObj;
            }
        }
        return null;
    }    

    function doSearch() {

        vm.tableParams.reload();
    }

}]);

});

