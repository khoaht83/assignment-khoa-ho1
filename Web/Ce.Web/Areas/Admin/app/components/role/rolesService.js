'use strict';

define(['app'], function (app) {

	var injectParams = ['$http', '$q'];

	var RolesFactory = function ($http, $q) {
		var serviceBase = 'api/role';
		var factory = {};

		factory.create = function (obj) {
			return $http.post(serviceBase + "/Create", obj).then(function (results) {
				obj.Id = results.Id;
				return results.Data;
			});
		}

		factory.update = function (obj) {
			return $http.put(serviceBase + "/Update", obj).then(function (results) {
				obj.Id = results.Id;
				return results.data;
			});
		}


		factory.delete = function (roleId) {
			return $http.delete(serviceBase + '/Delete/'+roleId).then(function (status) {
				return status;
			});
		}

		factory.getById = function (id) {
			return $http.get(serviceBase + '/GetByRoleId/' + id).then(function (results) {
				return results.data;
			});
		}

		factory.getAll = function () {
			return $http.get(serviceBase + '/GetAllRole').then(function (results) {
				return results;
			});
		}


		factory.search = function (roleCriteria) {

			return $http.post(serviceBase + "/Search", roleCriteria).then(function (response) {
				return response;
			});
		};


		return factory;

	}

	RolesFactory.$inject = injectParams;
	app.factory('rolesService', RolesFactory);

});
