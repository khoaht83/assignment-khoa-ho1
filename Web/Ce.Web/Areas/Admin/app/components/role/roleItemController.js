'use strict';

define(['app'], function (app) {
	app.controller('RoleItemController', ['$scope', '$location', '$stateParams'/*[@DependenciesService1]*/

						, '$timeout', 'rolesService', 'modalService',function ($scope, $location, $stateParams/*[@DependenciesService2]*/						

						, $timeout, rolesService, modalService) {

		var vm = this,
			roleId = ($stateParams.roleId) ? $stateParams.roleId : '',
			timer,
			onRouteChangeOff;

		vm.role = {};
		// for role Only		
		vm.formTitle = roleId == 0 ? 'Create role new' : 'Update role';		
		vm.isRepeat = true;

		/*get[@GetParentTables]*/	


		vm.saveRole = function () {
			if ($scope.itemForm.$valid) {
				if (!vm.role.Id) {
					rolesService.create(vm.role).then(processSuccess, processError);
				}
				else {
					rolesService.update(vm.role).then(processSuccess, processError);
				}
				$location.path('/roles');
			}
		};

		vm.delete = function (roleId) {
			var headerText = 'role';
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete Role',
				headerText: 'Delete ' + headerText + '?',
				bodyText: 'Do you want to delete role?'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					rolesService.delete(roleId).then(function () {
						onRouteChangeOff(); //Stop listening for location changes
						$location.path('/roles');
					}, processError);
				}
			});
		};

		function init() {

			if (roleId != '0') {
			    rolesService.getById(roleId).then(function (data) {
					vm.role = angular.copy(data);
				}, processError);
			}

			/*get[@CallGetParentTables]*/


			//Make sure they're warned if they made a change but didn't save it
			//Call to $on returns a "deregistration" function that can be called to
			//remove the listener (see routeChange() for an example of using it)
			onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

		}

		init();

		function routeChange(event, newUrl, oldUrl) {
			//Navigate to newUrl if the form isn't dirty
			if (!vm.itemForm || !vm.itemForm.$dirty) return;

			var modalOptions = {
				closeButtonText: 'Close',
				actionButtonText: 'Cancel',
				headerText: 'Data has changed!',
				bodyText: 'The data has changed, do you want to exist??'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					onRouteChangeOff(); //Stop listening for location changes
					$location.path($location.url(newUrl).hash()); //Go to page they're interested in
				}
			});

			//prevent navigation by default since we'll handle it
			//once the user selects a dialog option
			event.preventDefault();
			return;
		}

		function processSuccess() {
			ShowMessage({
				message: 'Success.'
			});
			if (vm.isRepeat) {
				vm.roleId = 0;
				vm.role = null;
				ClearTextFields();
			}
			else
			{
				$location.path('/roles');
			}
		}

		function processError(error) {
			ShowMessage({
				message: error.data.Message,
				type: 'danger'
			});
		}
	}]);

});

