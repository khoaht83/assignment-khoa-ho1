'use strict';

define(['app'], function (app) {

app.controller('RolesController', ['$scope', '$filter', '$location', 'NgTableParams',
                    '$timeout', 'rolesService', 'modalService',function ($scope, $filter, $location, NgTableParams,
    $timeout, rolesService, modalService) {

    var vm = this;
    vm.roles = [];
    vm.currentPage = 1;
    vm.itemPerPage = 10;
    vm.totalRecords = 0;
    vm.errorMessage = "";
    vm.sortDirection = "asc";
    vm.columnName = "name";

    vm.searchText = "";

	initalData();

    function processError(error) {
        vm.errorMessage = error.message;
        App.alert({
            message: error.data.Message,
            type: 'danger'
        });
    };


    vm.searchTextChanged = function () {
        doSearch();
    };


    function initalData() {
		vm.tableParams = new NgTableParams({
				page: 1,
				count: 10,
				filter: {
					ModelNum: ''
				},
				sorting:
				{
					ModelNum: 'asc'
				}
			},
			{
				getData: function ($defer, params) {
					var criteria = {
						CurrentPage: params.page() - 1,
						ItemPerPage: params.count(),
						SortColumn: vm.columnName,
						SortDirection: vm.sortDirection,
						Name: vm.searchText
					};

					rolesService.search(criteria).then(function (response) {

						vm.roles = response.data.Data;
						var orderedData = params.sorting() ?
							$filter('orderBy')(vm.roles, params.orderBy())
											: vm.roles;
						params.total(response.data.TotalRecords);
						$defer.resolve(orderedData);

					});


				}
			}, function (error) {
				console.log('errror', error);
			});
    }

	$scope.$on('doSearch', function (event, areaId) {
		initalData();
    });

    vm.edit = function (id) {
        $location.path('/role/' + id);
    };

    vm.delete = function (id) {

        var delObj = getRoleById(id);
        var objName = delObj.Name == undefined ? 'role' : delObj.Name;
		modalService.showMessageConfirm("Do you want to delete " + objName + " ?", function () {
                rolesService.delete(id).then(function () {
                    for (var i = 0; i < vm.roles.length; i++) {
                        if (vm.roles[i].Id === id) {
                            vm.roles.splice(i, 1);
							vm.tableParams.reload();
                            break;
                        }
                    }

                }, function (error) {
                    App.alert({
                        message: error.data.Message,
                        type: 'danger'
                    });
                });
        });
    };

    function getRoleById(id) {
        for (var i = 0; i < vm.roles.length; i++) {
            var delObj = vm.roles[i];
            if (delObj.Id === id) {
                return delObj;
            }
        }
        return null;
    }    

    function doSearch() {

        vm.tableParams.reload();
    }

}]);

});

