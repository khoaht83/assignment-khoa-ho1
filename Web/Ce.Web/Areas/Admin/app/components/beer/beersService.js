'use strict';

define(['app'], function (app) {

	var injectParams = ['$http', '$q'];

	var BeersFactory = function ($http, $q) {
		var serviceBase = 'api/beer';
		var factory = {};

		factory.create = function (obj) {
			return $http.post(serviceBase + "/Create", obj).then(function (results) {
				obj.Id = results.Id;
				return results.Data;
			});
		}

		factory.update = function (obj) {
			return $http.put(serviceBase + "/Update", obj).then(function (results) {
				obj.Id = results.Id;
				return results.data;
			});
		}


		factory.delete = function (beerId) {
			return $http.delete(serviceBase + '/Delete/'+beerId).then(function (status) {
				return status;
			});
		}

		factory.getById = function (id) {
			return $http.get(serviceBase + '/GetBeerById/' + id).then(function (results) {
				return results.data;
			});
		}

		factory.getAll = function () {
			return $http.get(serviceBase + '/GetAllBeer').then(function (results) {
				return results;
			});
		}


		factory.search = function (beerCriteria) {

			return $http.post(serviceBase + "/Search", beerCriteria).then(function (response) {
				return response;
			});
		};


		return factory;

	}

	BeersFactory.$inject = injectParams;
	app.factory('beersService', BeersFactory);

});
