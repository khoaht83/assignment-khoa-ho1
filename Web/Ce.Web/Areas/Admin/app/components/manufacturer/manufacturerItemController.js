'use strict';

define(['app'], function (app) {
    app.controller('ManufacturerItemController', ['$scope', '$location', '$stateParams', 'countriesService'

						, '$timeout', 'manufacturersService', 'modalService', function ($scope, $location, $stateParams, countriesService

						, $timeout, manufacturersService, modalService) {

						    var vm = this,
                                manufacturerId = ($stateParams.manufacturerId) ? $stateParams.manufacturerId : '',
                                timer,
                                onRouteChangeOff;

						    vm.manufacturer = {};
						    // for manufacturer Only		
						    vm.formTitle = manufacturerId == 0 ? 'Create new manufacturer' : 'Update manufacturer';
						    vm.isRepeat = true;
						    vm.countries = [];

						    /*get[@GetParentTables]*/
						    countriesService.getAll().then(function (response) {
						        vm.countries = response.data
						    }, processError);

						    vm.saveManufacturer = function () {
						        if ($scope.itemForm.$valid) {
						            if (!vm.manufacturer.Id) {
						                manufacturersService.create(vm.manufacturer).then(processSuccess, processError);
						            }
						            else {
						                manufacturersService.update(vm.manufacturer).then(processSuccess, processError);
						            }
						            $location.path('/manufacturers');
						        }
						    };

						    vm.delete = function (manufacturerId) {
						        var headerText = 'manufacturer';
						        var modalOptions = {
						            closeButtonText: 'Cancel',
						            actionButtonText: 'Delete Manufacturer',
						            headerText: 'Delete ' + headerText + '?',
						            bodyText: 'Do you want to delete manufacturer?'
						        };

						        modalService.showModal({}, modalOptions).then(function (result) {
						            if (result === 'ok') {
						                manufacturersService.delete(manufacturerId).then(function () {
						                    onRouteChangeOff(); //Stop listening for location changes
						                    $location.path('/manufacturers');
						                }, processError);
						            }
						        });
						    };

						    function init() {

						        if (manufacturerId != '0') {
						            manufacturersService.getById(manufacturerId).then(function (data) {
						                vm.manufacturer = angular.copy(data);
						            }, processError);
						        }

						        /*get[@CallGetParentTables]*/


						        //Make sure they're warned if they made a change but didn't save it
						        //Call to $on returns a "deregistration" function that can be called to
						        //remove the listener (see routeChange() for an example of using it)
						        onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

						    }

						    init();

						    function routeChange(event, newUrl, oldUrl) {
						        //Navigate to newUrl if the form isn't dirty
						        if (!vm.itemForm || !vm.itemForm.$dirty) return;

						        var modalOptions = {
						            closeButtonText: 'Close',
						            actionButtonText: 'Cancel',
						            headerText: 'Data has changed!',
						            bodyText: 'The data has changed, do you want to exist??'
						        };

						        modalService.showModal({}, modalOptions).then(function (result) {
						            if (result === 'ok') {
						                onRouteChangeOff(); //Stop listening for location changes
						                $location.path($location.url(newUrl).hash()); //Go to page they're interested in
						            }
						        });

						        //prevent navigation by default since we'll handle it
						        //once the user selects a dialog option
						        event.preventDefault();
						        return;
						    }

						    function processSuccess() {
						        ShowMessage({
						            message: 'Success.'
						        });
						        if (vm.isRepeat) {
						            vm.manufacturerId = 0;
						            vm.manufacturer = null;
						            ClearTextFields();
						        }
						        else {
						            $location.path('/manufacturers');
						        }
						    }

						    function processError(error) {
						        ShowMessage({
						            message: error.data.Message,
						            type: 'danger'
						        });
						    }
						}]);

});

