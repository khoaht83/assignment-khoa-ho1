'use strict';

define(['app'], function (app) {

    app.controller('ManufacturersController', ['$scope', '$filter', '$location', 'NgTableParams',
                        '$timeout', 'manufacturersService', 'modalService', function ($scope, $filter, $location, NgTableParams,
        $timeout, manufacturersService, modalService) {

                            var vm = this;
                            vm.manufacturers = [];
                            vm.currentPage = 1;
                            vm.itemPerPage = 10;
                            vm.totalRecords = 0;
                            vm.errorMessage = "";
                            vm.sortDirection = "asc";
                            vm.columnName = "name";

                            vm.searchText = "";

                            initalData();

                            function processError(error) {
                                vm.errorMessage = error.message;
                                App.alert({
                                    message: error.data.Message,
                                    type: 'danger'
                                });
                            };


                            vm.searchTextChanged = function () {
                                doSearch();
                            };


                            function initalData() {
                                vm.tableParams = new NgTableParams({
                                    page: 1,
                                    count: 10,
                                    filter: {
                                        ModelNum: ''
                                    },
                                    sorting:
                                    {
                                        ModelNum: 'asc'
                                    }
                                },
                                    {
                                        getData: function ($defer, params) {
                                            var criteria = {
                                                CurrentPage: params.page() - 1,
                                                ItemPerPage: params.count(),
                                                SortColumn: vm.columnName,
                                                SortDirection: vm.sortDirection,
                                                Name: vm.searchText
                                            };

                                            manufacturersService.search(criteria).then(function (response) {

                                                vm.manufacturers = response.data.Data;
                                                var orderedData = params.sorting() ?
                                                    $filter('orderBy')(vm.manufacturers, params.orderBy())
                                                                    : vm.manufacturers;
                                                params.total(response.data.TotalRecords);
                                                $defer.resolve(orderedData);

                                            });


                                        }
                                    }, function (error) {
                                        console.log('errror', error);
                                    });
                            }

                            $scope.$on('doSearch', function (event, areaId) {
                                initalData();
                            });

                            vm.edit = function (id) {
                                $location.path('/manufacturer/' + id);
                            };

                            vm.delete = function (id) {

                                var delObj = getManufacturerById(id);
                                var objName = delObj.Name == undefined ? 'manufacturer' : delObj.Name;
                                modalService.showMessageConfirm("Do you want to delete " + objName + " ?", function () {
                                    manufacturersService.delete(id).then(function () {
                                        for (var i = 0; i < vm.manufacturers.length; i++) {
                                            if (vm.manufacturers[i].Id === id) {
                                                vm.manufacturers.splice(i, 1);
                                                vm.tableParams.reload();
                                                break;
                                            }
                                        }

                                    }, function (error) {
                                        App.alert({
                                            message: error.data.Message,
                                            type: 'danger'
                                        });
                                    });
                                });
                            };

                            function getManufacturerById(id) {
                                for (var i = 0; i < vm.manufacturers.length; i++) {
                                    var delObj = vm.manufacturers[i];
                                    if (delObj.Id === id) {
                                        return delObj;
                                    }
                                }
                                return null;
                            }

                            function doSearch() {

                                vm.tableParams.reload();
                            }

                        }]);

});

