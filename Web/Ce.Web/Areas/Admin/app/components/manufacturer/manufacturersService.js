'use strict';

define(['app'], function (app) {

	var injectParams = ['$http', '$q'];

	var ManufacturersFactory = function ($http, $q) {
		var serviceBase = 'api/manufacturer';
		var factory = {};

		factory.create = function (obj) {
			return $http.post(serviceBase + "/Create", obj).then(function (results) {
				obj.Id = results.Id;
				return results.Data;
			});
		}

		factory.update = function (obj) {
			return $http.put(serviceBase + "/Update", obj).then(function (results) {
				obj.Id = results.Id;
				return results.data;
			});
		}


		factory.delete = function (manufacturerId) {
			return $http.delete(serviceBase + '/Delete/'+manufacturerId).then(function (status) {
				return status;
			});
		}

		factory.getById = function (id) {
			return $http.get(serviceBase + '/GetManufacturerById/' + id).then(function (results) {
				return results.data;
			});
		}

		factory.getAll = function () {
			return $http.get(serviceBase + '/GetAllManufacturer').then(function (results) {
				return results;
			});
		}


		factory.search = function (manufacturerCriteria) {

			return $http.post(serviceBase + "/Search", manufacturerCriteria).then(function (response) {
				return response;
			});
		};


		return factory;

	}

	ManufacturersFactory.$inject = injectParams;
	app.factory('manufacturersService', ManufacturersFactory);

});
