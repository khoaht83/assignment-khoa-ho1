'use strict';

define(['app'], function (app) {

app.controller('UsersController', ['$scope', '$filter', '$location', 'NgTableParams',
                    '$timeout', 'usersService', 'modalService',function ($scope, $filter, $location, NgTableParams,
    $timeout, usersService, modalService) {

    var vm = this;
    vm.users = [];
    vm.currentPage = 1;
    vm.itemPerPage = 10;
    vm.totalRecords = 0;
    vm.errorMessage = "";
    vm.sortDirection = "asc";
    vm.columnName = "name";

    vm.searchText = "";

	initalData();

    function processError(error) {
        vm.errorMessage = error.message;
        App.alert({
            message: error.data.Message,
            type: 'danger'
        });
    };


    vm.searchTextChanged = function () {
        doSearch();
    };


    function initalData() {
		vm.tableParams = new NgTableParams({
				page: 1,
				count: 10,
				filter: {
					ModelNum: ''
				},
				sorting:
				{
					ModelNum: 'asc'
				}
			},
			{
				getData: function ($defer, params) {
					var criteria = {
						CurrentPage: params.page() - 1,
						ItemPerPage: params.count(),
						SortColumn: vm.columnName,
						SortDirection: vm.sortDirection,
						Name: vm.searchText
					};

					usersService.search(criteria).then(function (response) {

						vm.users = response.data.Data;
						var orderedData = params.sorting() ?
							$filter('orderBy')(vm.users, params.orderBy())
											: vm.users;
						params.total(response.data.TotalRecords);
						$defer.resolve(orderedData);

					});


				}
			}, function (error) {
				console.log('errror', error);
			});
    }

	$scope.$on('doSearch', function (event, areaId) {
		initalData();
    });

    vm.edit = function (id) {
        $location.path('/user/' + id);
    };

    vm.delete = function (id) {

        var delObj = getUserById(id);
        var objName = delObj.Name == undefined ? 'user' : delObj.Name;
		modalService.showMessageConfirm("Do you want to delete " + objName + " ?", function () {
                usersService.delete(id).then(function () {
                    for (var i = 0; i < vm.users.length; i++) {
                        if (vm.users[i].Id === id) {
                            vm.users.splice(i, 1);
							vm.tableParams.reload();
                            break;
                        }
                    }

                }, function (error) {
                    App.alert({
                        message: error.data.Message,
                        type: 'danger'
                    });
                });
        });
    };

    function getUserById(id) {
        for (var i = 0; i < vm.users.length; i++) {
            var delObj = vm.users[i];
            if (delObj.Id === id) {
                return delObj;
            }
        }
        return null;
    }    

    function doSearch() {

        vm.tableParams.reload();
    }

}]);

});

