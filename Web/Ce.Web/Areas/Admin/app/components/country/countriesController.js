'use strict';

define(['app'], function (app) {

app.controller('CountriesController', ['$scope', '$filter', '$location', 'NgTableParams',
                    '$timeout', 'countriesService', 'modalService',function ($scope, $filter, $location, NgTableParams,
    $timeout, countriesService, modalService) {

    var vm = this;
    vm.countries = [];
    vm.currentPage = 1;
    vm.itemPerPage = 10;
    vm.totalRecords = 0;
    vm.errorMessage = "";
    vm.sortDirection = "asc";
    vm.columnName = "name";

    vm.searchText = "";

	initalData();

    function processError(error) {
        vm.errorMessage = error.message;
        App.alert({
            message: error.data.Message,
            type: 'danger'
        });
    };


    vm.searchTextChanged = function () {
        doSearch();
    };


    function initalData() {
		vm.tableParams = new NgTableParams({
				page: 1,
				count: 10,
				filter: {
					ModelNum: ''
				},
				sorting:
				{
					ModelNum: 'asc'
				}
			},
			{
				getData: function ($defer, params) {
					var criteria = {
						CurrentPage: params.page() - 1,
						ItemPerPage: params.count(),
						SortColumn: vm.columnName,
						SortDirection: vm.sortDirection,
						Name: vm.searchText
					};

					countriesService.search(criteria).then(function (response) {

						vm.countries = response.data.Data;
						var orderedData = params.sorting() ?
							$filter('orderBy')(vm.countries, params.orderBy())
											: vm.countries;
						params.total(response.data.TotalRecords);
						$defer.resolve(orderedData);

					});


				}
			}, function (error) {
				console.log('errror', error);
			});
    }

	$scope.$on('doSearch', function (event, areaId) {
		initalData();
    });

    vm.edit = function (id) {
        $location.path('/country/' + id);
    };

    vm.delete = function (id) {

        var delObj = getCountryById(id);
        var objName = delObj.Name == undefined ? 'country' : delObj.Name;
		modalService.showMessageConfirm("Do you want to delete " + objName + " ?", function () {
                countriesService.delete(id).then(function () {
                    for (var i = 0; i < vm.countries.length; i++) {
                        if (vm.countries[i].Id === id) {
                            vm.countries.splice(i, 1);
							vm.tableParams.reload();
                            break;
                        }
                    }

                }, function (error) {
                    App.alert({
                        message: error.data.Message,
                        type: 'danger'
                    });
                });
        });
    };

    function getCountryById(id) {
        for (var i = 0; i < vm.countries.length; i++) {
            var delObj = vm.countries[i];
            if (delObj.Id === id) {
                return delObj;
            }
        }
        return null;
    }    

    function doSearch() {

        vm.tableParams.reload();
    }

}]);

});

