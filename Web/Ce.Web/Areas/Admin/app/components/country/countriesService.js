'use strict';

define(['app'], function (app) {

	var injectParams = ['$http', '$q'];

	var CountriesFactory = function ($http, $q) {
		var serviceBase = 'api/country';
		var factory = {};

		factory.create = function (obj) {
			return $http.post(serviceBase + "/Create", obj).then(function (results) {
				obj.Id = results.Id;
				return results.Data;
			});
		}

		factory.update = function (obj) {
			return $http.put(serviceBase + "/Update", obj).then(function (results) {
				obj.Id = results.Id;
				return results.data;
			});
		}


		factory.delete = function (countryId) {
			return $http.delete(serviceBase + '/Delete/'+countryId).then(function (status) {
				return status;
			});
		}

		factory.getById = function (id) {
			return $http.get(serviceBase + '/GetCountryById/' + id).then(function (results) {
				return results.data;
			});
		}

		factory.getAll = function () {
			return $http.get(serviceBase + '/GetAllCountry').then(function (results) {
				return results;
			});
		}


		factory.search = function (countryCriteria) {

			return $http.post(serviceBase + "/Search", countryCriteria).then(function (response) {
				return response;
			});
		};


		return factory;

	}

	CountriesFactory.$inject = injectParams;
	app.factory('countriesService', CountriesFactory);

});
