'use strict';

define(['app'], function (app) {

	var injectParams = ['$http', '$q'];

	var BeerCategoriesFactory = function ($http, $q) {
		var serviceBase = 'api/beercategory';
		var factory = {};

		factory.create = function (obj) {
			return $http.post(serviceBase + "/Create", obj).then(function (results) {
				obj.Id = results.Id;
				return results.Data;
			});
		}

		factory.update = function (obj) {
			return $http.put(serviceBase + "/Update", obj).then(function (results) {
				obj.Id = results.Id;
				return results.data;
			});
		}


		factory.delete = function (beercategoryId) {
			return $http.delete(serviceBase + '/Delete/'+beercategoryId).then(function (status) {
				return status;
			});
		}

		factory.getById = function (id) {
			return $http.get(serviceBase + '/GetBeerCategoryById/' + id).then(function (results) {
				return results.data;
			});
		}

		factory.getAll = function () {
			return $http.get(serviceBase + '/GetAllBeerCategory').then(function (results) {
				return results;
			});
		}


		factory.search = function (beercategoryCriteria) {

			return $http.post(serviceBase + "/Search", beercategoryCriteria).then(function (response) {
				return response;
			});
		};


		return factory;

	}

	BeerCategoriesFactory.$inject = injectParams;
	app.factory('beerCategoriesService', BeerCategoriesFactory);

});
