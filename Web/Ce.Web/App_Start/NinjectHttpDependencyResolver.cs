﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using Ninject;

namespace Ce.Web
{
    public class NinjectHttpDependencyResolver : IDependencyResolver, IDependencyScope
    {
        private readonly IKernel _kernel;
        private static IKernel objKernel;
        public NinjectHttpDependencyResolver(IKernel kernel)
        {
            objKernel = kernel;
            _kernel = kernel;
        }
        public IDependencyScope BeginScope()
        {
            return this;
        }

        public void Dispose()
        {
            //Do nothing
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
                return _kernel.GetAll(serviceType);

        }

        public static T GetService<T>()
        {
            return objKernel.TryGet<T>();
        }
    }
}