﻿using Fw.Core.DataContext;
using Fw.Core.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Fw.Core.UnitOfWork;

namespace Fw.Core.Repository
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly DbSet<TEntity> dbSet;
        private DbContext dbContext;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public Repository(IDataContext context)
        {
            dbContext = context as DbContext;
            if (dbContext != null)
            {
                dbSet = dbContext.Set<TEntity>();
            }

        }


        /// <summary>
        /// Add
        /// </summary>
        /// <param name="entity"></param>
        public void Add(TEntity entity)
        {
            dbSet.Add(entity);
        }



        /// <summary>
        /// Add
        /// </summary>
        /// <param name="entity"></param>
        public void Update(TEntity entity)
        {
            dbContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            
        }


        /// <summary>
        /// Add range
        /// </summary>
        /// <param name="entities"></param>
        public void AddRange(IEnumerable<TEntity> entities)
        {
            dbSet.AddRange(entities);
        }


        /// <summary>
        /// Find entities
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return dbSet.Where(predicate);
        }


        /// <summary>
        /// Get by Id as guid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TEntity GetById(Guid id)
        {
            return dbSet.Find(id);
        }

        /// <summary>
        /// get all entites
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> GetAll()
        {
            return dbSet.AsQueryable();
        }


        /// <summary>
        /// Remove an entity
        /// </summary>
        /// <param name="entity"></param>
        public void Remove(TEntity entity)
        {
            dbSet.Remove(entity);
        }

        /// <summary>
        /// Remove range
        /// </summary>
        /// <param name="entities"></param>
        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            dbSet.RemoveRange(entities);
        }

    }
}
