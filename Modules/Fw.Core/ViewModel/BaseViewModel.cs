﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Fw.Core.ViewModel
{
    public abstract class BaseViewModel
    {
        
        public Guid Id { get; set; }
        public bool Status { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }
    }

}
