﻿using Fw.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Fw.Core.Service
{
    public interface IService<TEntity,TView> where TEntity 
        : class where TView : BaseViewModel
    {

        TView GetById(Guid id);
        TEntity Get(Guid id);
        IEnumerable<TView> GetAll();
        IEnumerable<TView> GetAll(Guid? areaId);
        OperationStatus Add(TView entity);
        OperationStatus AddRange(IEnumerable<TView> entities);
        OperationStatus Update(TView entity);
        OperationStatus Remove(Guid id);
        OperationStatus Remove(TView entity);        
        IQueryable<TView> Search(Expression<Func<TEntity, bool>> query,out int totalResult);
    }
}
