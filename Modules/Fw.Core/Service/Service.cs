﻿using Fw.Core.Repository;
using Fw.Core.UnitOfWork;
using Fw.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;

namespace Fw.Core.Service
{

    public class Service<TEntity, TView> : IService<TEntity, TView>
        where TEntity : class
        where TView : BaseViewModel
    {
        #region Private Fields
        protected IRepository<TEntity> repository;
        protected IUnitOfWork unitOfWork;
        #endregion Private Fields

        #region Constructor
        protected Service(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.repository = unitOfWork.Repository<TEntity>();
        }
        #endregion Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public virtual OperationStatus Add(TView entity)
        {
            var opStatus = new OperationStatus { Status = true };
            try
            {
                entity.Id = Guid.NewGuid();
                entity.CreatedDate = entity.UpdatedDate = DateTime.UtcNow;
                entity.Status = true;
                var obj = AutoMapper.Mapper.Map<TView, TEntity>(entity);
                repository.Add(obj);
                unitOfWork.Complete();
            }
            catch (Exception exp)
            {
                opStatus.Status = false;
                opStatus.ExceptionMessage = exp.Message;
            }

            return opStatus;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        public virtual OperationStatus AddRange(IEnumerable<TView> entities)
        {
            var opStatus = new OperationStatus { Status = true };
            try
            {
                repository.AddRange(entities.Select(t => AutoMapper.Mapper.Map<TView, TEntity>(t)));
                unitOfWork.Complete();
            }
            catch (Exception exp)
            {
                opStatus.Status = false;
                opStatus.ExceptionMessage = exp.Message;
            }

            return opStatus;


        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TView GetById(Guid id)
        {
            var obj = repository.GetById(id);
            var vm = AutoMapper.Mapper.Map<TEntity, TView>(obj);
            return vm;
        }

        public virtual TEntity Get(Guid id)
        {
            var obj = repository.GetById(id);
            return obj;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TView> GetAll()
        {
            var entities = repository
                                    .GetAll()
                                    .ToList()
                                    .Select(t => AutoMapper.Mapper.Map<TEntity, TView>(t))
                                    .Where(t => t.Status)
                                    .ToList();
            return entities;

        }


        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="areaId">The area identifier.</param>
        /// <returns></returns>
        public virtual IEnumerable<TView> GetAll(Guid? areaId)
        {
            var entities = repository.GetAll()
                                    .ToList()
                                    .Select(t => AutoMapper.Mapper.Map<TEntity, TView>(t));

            return entities;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public virtual IQueryable<TView> Search(Expression<Func<TEntity, bool>> query, out int totalResult)
        {
            var result = repository.Find(query);
            totalResult = result.Count();
            return result.Select(t => AutoMapper.Mapper.Map<TEntity, TView>(t));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public OperationStatus Remove(TView view)
        {
            var opStatus = new OperationStatus { Status = true };
            try
            {
                var entity = AutoMapper.Mapper.Map<TView, TEntity>(view);
                repository.Remove(entity);
                unitOfWork.Complete();
            }
            catch (Exception exp)
            {
                opStatus.Status = false;
                opStatus.ExceptionMessage = exp.Message;
            }

            return opStatus;


        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public OperationStatus Remove(Guid id)
        {
            var opStatus = new OperationStatus { Status = true };
            try
            {

                var entity = Get(id);
                repository.Remove(entity);
                unitOfWork.Complete();
            }
            catch (Exception exp)
            {
                opStatus.Status = false;
                opStatus.ExceptionMessage = exp.Message;
            }

            return opStatus;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public virtual OperationStatus Update(TView view)
        {

            var opStatus = new OperationStatus { Status = true };
            try
            {
                view.UpdatedDate = DateTime.UtcNow;
                var entity = AutoMapper.Mapper.Map<TView, TEntity>(view);
                repository.Update(entity);
                unitOfWork.Complete();
            }
            catch (Exception exp)
            {
                opStatus.Status = false;
                opStatus.ExceptionMessage = exp.Message;
            }

            return opStatus;



        }

    }
}
