using Ce.Infrastructure.Domain;
using Ce.Infrastructure.ViewModel;
using Fw.Core.Service;
using Fw.Core.UnitOfWork;
using System.Collections.Generic;
using System;
using Ce.Infrastructure.Criteria;
using System.Linq;
using System.Linq.Expressions;

namespace Ce.Infrastructure.Services
{

 /// <summary>
    /// Manufacturer service interface
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    public interface IManufacturerService : IService<Manufacturer, ManufacturerViewModel>
    {
        IEnumerable<ManufacturerViewModel> Search(ManufacturerCriteria criteria, out int totalRecords);        
    }

    /// <summary>
    /// Manufacturer service
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    public partial class ManufacturerService :  Service<Manufacturer, ManufacturerViewModel>, IManufacturerService
    {
  
        #region constructors

        public ManufacturerService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        #endregion

        #region public methods

        public IEnumerable<ManufacturerViewModel> Search(ManufacturerCriteria criteria, out int totalRecords)
        {

            Expression<Func<Manufacturer, bool>> predicate = t =>
						((criteria.Id==null || criteria.Id == Guid.Empty || t.Id == criteria.Id.Value )
							&&(string.IsNullOrEmpty(criteria.Name)||( t.Name.Contains(criteria.Name) || criteria.Name.Contains(t.Name) ))
							&&(criteria.CountryId==null || criteria.CountryId == Guid.Empty || t.CountryId == criteria.CountryId.Value )
							&&(string.IsNullOrEmpty(criteria.CreatedBy)||( t.CreatedBy.Contains(criteria.CreatedBy) || criteria.CreatedBy.Contains(t.CreatedBy) ))
							&&(string.IsNullOrEmpty(criteria.UpdatedBy)||( t.UpdatedBy.Contains(criteria.UpdatedBy) || criteria.UpdatedBy.Contains(t.UpdatedBy) ))
							&&(criteria.CreatedDate==null || t.CreatedDate.CompareTo(criteria.CreatedDate.Value) == 0  )
							&&(criteria.UpdatedDate==null || t.UpdatedDate.CompareTo(criteria.UpdatedDate.Value) == 0  )
							&&(criteria.Status==null || t.Status == criteria.Status.Value )
						)
							;

            var query = unitOfWork.Repository<Manufacturer>().Find(predicate);

            totalRecords = query.Count();

            criteria.SortColumn = string.IsNullOrEmpty(criteria.SortColumn) ? string.Empty : criteria.SortColumn.ToLower();
            bool isAsc = criteria.SortDirection.ToLower().Equals("asc");

			switch (criteria.SortColumn){
				case "name" :
					query = isAsc ? query.OrderBy(t => t.Name) : query.OrderByDescending(t => t.Name);
					break;
				case "createdby" :
					query = isAsc ? query.OrderBy(t => t.CreatedBy) : query.OrderByDescending(t => t.CreatedBy);
					break;
				case "updatedby" :
					query = isAsc ? query.OrderBy(t => t.UpdatedBy) : query.OrderByDescending(t => t.UpdatedBy);
					break;
				case "createddate" :
					query = isAsc ? query.OrderBy(t => t.CreatedDate) : query.OrderByDescending(t => t.CreatedDate);
					break;
				case "updateddate" :
					query = isAsc ? query.OrderBy(t => t.UpdatedDate) : query.OrderByDescending(t => t.UpdatedDate);
					break;
				default: break;}

            query = query.Skip(criteria.CurrentPage * criteria.ItemPerPage).Take(criteria.ItemPerPage);

            return query.ToList().Select(t => AutoMapper.Mapper.Map<Manufacturer, ManufacturerViewModel>(t)).ToList();
        }
        

        #endregion
    }
}
