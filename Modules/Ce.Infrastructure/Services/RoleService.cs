using Ce.Infrastructure.Domain;
using Ce.Infrastructure.ViewModel;
using Fw.Core.Service;
using Fw.Core.UnitOfWork;
using System.Collections.Generic;
using System;
using Ce.Infrastructure.Criteria;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNet.Identity.EntityFramework;
using Fw.Core.DataContext;
using Ce.Infrastructure.DatabaseContext;

namespace Ce.Infrastructure.Services
{

 /// <summary>
    /// Role service interface
    /// </summary>
    public interface IRoleService : IService<Role, RoleViewModel>
    {
        IEnumerable<RoleViewModel> Search(RoleCriteria criteria, out int totalRecords);        
    }

    /// <summary>
    /// Role service
    /// </summary>
    public partial class RoleService :  Service<Role, RoleViewModel>, IRoleService
    {
  
        #region constructors

        public RoleService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            
        }
        #endregion

        #region public methods

        public IEnumerable<RoleViewModel> Search(RoleCriteria criteria, out int totalRecords)
        {

            Expression<Func<Role, bool>> predicate = t =>
						((string.IsNullOrEmpty(criteria.Id)||( t.Id.Contains(criteria.Id) || criteria.Id.Contains(t.Id) ))
							&&(string.IsNullOrEmpty(criteria.Name)||( t.Name.Contains(criteria.Name) || criteria.Name.Contains(t.Name) ))
						)
							;

            var query = unitOfWork.Repository<Role>().Find(predicate);

            totalRecords = query.Count();

            criteria.SortColumn = string.IsNullOrEmpty(criteria.SortColumn) ? string.Empty : criteria.SortColumn.ToLower();
            bool isAsc = criteria.SortDirection.ToLower().Equals("asc");

			switch (criteria.SortColumn){
				case "id" :
					query = isAsc ? query.OrderBy(t => t.Id) : query.OrderByDescending(t => t.Id);
					break;
				case "name" :
					query = isAsc ? query.OrderBy(t => t.Name) : query.OrderByDescending(t => t.Name);
					break;
				default: break;}

            query = query.Skip(criteria.CurrentPage * criteria.ItemPerPage).Take(criteria.ItemPerPage);

            return query.ToList().Select(t => AutoMapper.Mapper.Map<Role, RoleViewModel>(t)).ToList();
        }
        

        #endregion
    }
}
