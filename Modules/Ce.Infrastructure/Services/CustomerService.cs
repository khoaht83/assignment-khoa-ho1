using Ce.Infrastructure.Domain;
using Ce.Infrastructure.ViewModel;
using Fw.Core.Service;
using Fw.Core.UnitOfWork;
using System.Collections.Generic;
using System;
using Ce.Infrastructure.Criteria;
using System.Linq;
using System.Linq.Expressions;
using Fw.Core.DataContext;
using Ce.Infrastructure.DatabaseContext;

namespace Ce.Infrastructure.Services
{

 /// <summary>
    /// User service interface
    /// </summary>
    public interface ICustomerService : IService<Customer, CustomerViewModel>
    {
        bool AddPassport(Guid beerCateoryId);
        IEnumerable<CustomerViewModel> Search(CustomerCriteria criteria, out int totalRecords);        
    }

    /// <summary>
    /// User service
    /// </summary>
    public partial class CustomerService : Service<Customer, CustomerViewModel>, ICustomerService
    {
  
        #region constructors

        public CustomerService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion

        #region public methods

        public IEnumerable<CustomerViewModel> Search(CustomerCriteria criteria, out int totalRecords)
        {

            Expression<Func<Customer, bool>> predicate = t =>
						((string.IsNullOrEmpty(criteria.Id)||( t.Id.Contains(criteria.Id) || criteria.Id.Contains(t.Id) ))
							&&(string.IsNullOrEmpty(criteria.Email)||( t.Email.Contains(criteria.Email) || criteria.Email.Contains(t.Email) ))
							&&(criteria.EmailConfirmed==null || t.EmailConfirmed == criteria.EmailConfirmed.Value )
							&&(string.IsNullOrEmpty(criteria.PasswordHash)||( t.PasswordHash.Contains(criteria.PasswordHash) || criteria.PasswordHash.Contains(t.PasswordHash) ))
							&&(string.IsNullOrEmpty(criteria.SecurityStamp)||( t.SecurityStamp.Contains(criteria.SecurityStamp) || criteria.SecurityStamp.Contains(t.SecurityStamp) ))
							&&(string.IsNullOrEmpty(criteria.PhoneNumber)||( t.PhoneNumber.Contains(criteria.PhoneNumber) || criteria.PhoneNumber.Contains(t.PhoneNumber) ))
							&&(criteria.PhoneNumberConfirmed==null || t.PhoneNumberConfirmed == criteria.PhoneNumberConfirmed.Value )
							&&(criteria.TwoFactorEnabled==null || t.TwoFactorEnabled == criteria.TwoFactorEnabled.Value )
							&&(criteria.LockoutEndDateUtc==null || t.LockoutEndDateUtc.Value.CompareTo(criteria.LockoutEndDateUtc.Value) == 0  )
							&&(criteria.LockoutEnabled==null || t.LockoutEnabled == criteria.LockoutEnabled.Value )
							&&(criteria.AccessFailedCount==null || t.AccessFailedCount.Equals(criteria.AccessFailedCount.Value) )
							&&(string.IsNullOrEmpty(criteria.UserName)||( t.UserName.Contains(criteria.UserName) || criteria.UserName.Contains(t.UserName) ))
						)
							;

            var query = unitOfWork.Repository<Customer>().Find(predicate);

            totalRecords = query.Count();

            criteria.SortColumn = string.IsNullOrEmpty(criteria.SortColumn) ? string.Empty : criteria.SortColumn.ToLower();
            bool isAsc = criteria.SortDirection.ToLower().Equals("asc");

			switch (criteria.SortColumn){
				case "id" :
					query = isAsc ? query.OrderBy(t => t.Id) : query.OrderByDescending(t => t.Id);
					break;
				case "email" :
					query = isAsc ? query.OrderBy(t => t.Email) : query.OrderByDescending(t => t.Email);
					break;
				case "passwordhash" :
					query = isAsc ? query.OrderBy(t => t.PasswordHash) : query.OrderByDescending(t => t.PasswordHash);
					break;
				case "securitystamp" :
					query = isAsc ? query.OrderBy(t => t.SecurityStamp) : query.OrderByDescending(t => t.SecurityStamp);
					break;
				case "phonenumber" :
					query = isAsc ? query.OrderBy(t => t.PhoneNumber) : query.OrderByDescending(t => t.PhoneNumber);
					break;
				case "lockoutenddateutc" :
					query = isAsc ? query.OrderBy(t => t.LockoutEndDateUtc) : query.OrderByDescending(t => t.LockoutEndDateUtc);
					break;
				case "username" :
					query = isAsc ? query.OrderBy(t => t.UserName) : query.OrderByDescending(t => t.UserName);
					break;
				default: break;}

            query = query.Skip(criteria.CurrentPage * criteria.ItemPerPage).Take(criteria.ItemPerPage);

            return query.ToList().Select(t => AutoMapper.Mapper.Map<Customer, CustomerViewModel>(t)).ToList();
        }
        

        #endregion

        public bool AddPassport(Guid beerCateoryId)
        {
            throw new NotImplementedException();
        }


    }
}
