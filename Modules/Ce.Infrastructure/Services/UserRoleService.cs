using Ce.Infrastructure.Domain;
using Ce.Infrastructure.ViewModel;
using Fw.Core.Service;
using Fw.Core.UnitOfWork;
using System.Collections.Generic;
using System;
using Ce.Infrastructure.Criteria;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNet.Identity.EntityFramework;
using Fw.Core.DataContext;
using Ce.Infrastructure.DatabaseContext;

namespace Ce.Infrastructure.Services
{

 /// <summary>
    /// UserRole service interface
    /// </summary>
    public interface IUserRoleService : IService<UserRole, UserRoleViewModel>
    {
        IEnumerable<UserRoleViewModel> Search(UserRoleCriteria criteria, out int totalRecords);        
    }

    /// <summary>
    /// UserRole service
    /// </summary>
    public partial class UserRoleService :  Service<UserRole, UserRoleViewModel>, IUserRoleService
    {
  
        #region constructors

        public UserRoleService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        #endregion

        #region public methods

        public IEnumerable<UserRoleViewModel> Search(UserRoleCriteria criteria, out int totalRecords)
        {

            Expression<Func<UserRole, bool>> predicate = t =>
						((string.IsNullOrEmpty(criteria.UserId)||( t.UserId.Contains(criteria.UserId) || criteria.UserId.Contains(t.UserId) ))
							&&(string.IsNullOrEmpty(criteria.RoleId)||( t.RoleId.Contains(criteria.RoleId) || criteria.RoleId.Contains(t.RoleId) ))
						)
							;

            var query = unitOfWork.Repository<UserRole>().Find(predicate);

            totalRecords = query.Count();

            criteria.SortColumn = string.IsNullOrEmpty(criteria.SortColumn) ? string.Empty : criteria.SortColumn.ToLower();
            bool isAsc = criteria.SortDirection.ToLower().Equals("asc");

			switch (criteria.SortColumn){
				case "userid" :
					query = isAsc ? query.OrderBy(t => t.UserId) : query.OrderByDescending(t => t.UserId);
					break;
				case "roleid" :
					query = isAsc ? query.OrderBy(t => t.RoleId) : query.OrderByDescending(t => t.RoleId);
					break;
				default: break;}

            query = query.Skip(criteria.CurrentPage * criteria.ItemPerPage).Take(criteria.ItemPerPage);

            return query.ToList().Select(t => AutoMapper.Mapper.Map<UserRole, UserRoleViewModel>(t)).ToList();
        }
        

        #endregion
    }
}
