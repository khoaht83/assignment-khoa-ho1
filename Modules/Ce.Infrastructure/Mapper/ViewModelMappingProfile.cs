﻿using Ce.Infrastructure.Domain;
using AutoMapper;
using Ce.Infrastructure.ViewModel;

namespace Ce.Infrastructure
{
    public class ViewModelMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Beer, BeerViewModel>().ReverseMap();
            Mapper.CreateMap<BeerCategory, BeerCategoryViewModel>().ReverseMap();
            Mapper.CreateMap<Country, CountryViewModel>().ReverseMap();
            Mapper.CreateMap<Manufacturer, ManufacturerViewModel>().ReverseMap();
            Mapper.CreateMap<Customer, CustomerViewModel>().ReverseMap();
            Mapper.CreateMap<CustomerPassport, CustomerPassportViewModel>().ReverseMap();
            Mapper.CreateMap<User, UserViewModel>().ReverseMap();
            Mapper.CreateMap<UserRole, UserRoleViewModel>().ReverseMap();
            Mapper.CreateMap<Role, RoleViewModel>().ReverseMap();
        }
    }
}

