using Fw.Core.Criteria;
using System;

namespace Ce.Infrastructure.Criteria
{
    /// <summary>
    /// Role criteria
    /// </summary>
    public class RoleCriteria : BaseCriteria
    {
			public string Id {get;set;}
			public string Name {get;set;}
    }
}
