using System;
using System.ComponentModel.DataAnnotations;
using Fw.Core.ViewModel;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// Manufacturer View Model
    /// Created by KhoaHT
	/// Created Date: Friday, April 29, 2016
    /// </summary>
    public class ManufacturerViewModel : BaseViewModel
    {

			public string Name {get;set;}
			[Required]
			public Guid CountryId {get;set;}

    }
}
