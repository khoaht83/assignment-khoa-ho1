using System;
using System.ComponentModel.DataAnnotations;
using Fw.Core.ViewModel;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// Beer View Model
    /// Created by KhoaHT
	/// Created Date: Friday, April 29, 2016
    /// </summary>
    public class BeerViewModel : BaseViewModel
    {
			[Required]
			public Guid BeerCategoryId {get;set;}
			public string Name {get;set;}
			[Required]
			public bool IsArchived {get;set;}
			[Required]
			public decimal Price {get;set;}
			public string Description {get;set;}

			public Guid ManufacturerId {get;set;}
    }
}
