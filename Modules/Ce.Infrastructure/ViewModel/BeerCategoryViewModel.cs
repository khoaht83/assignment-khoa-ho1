using System;
using System.ComponentModel.DataAnnotations;
using Fw.Core.ViewModel;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// BeerCategory View Model
    /// Created by KhoaHT
	/// Created Date: Friday, April 29, 2016
    /// </summary>
    public class BeerCategoryViewModel : BaseViewModel
    {
			[Required]
			public string Name {get;set;}
    }
}
