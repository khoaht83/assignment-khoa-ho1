using Fw.Core.ViewModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// UserRole View Model
    /// </summary>
    public class UserRoleViewModel : BaseViewModel
    {
			[Required]
			public string UserId {get;set;}
			[Required]
			public string RoleId {get;set;}
			public string IdentityUser_Id {get;set;}
    }
}
