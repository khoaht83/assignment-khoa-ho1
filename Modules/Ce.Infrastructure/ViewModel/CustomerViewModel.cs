using Fw.Core.ViewModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// User View Model
    /// </summary>
    public class CustomerViewModel : BaseViewModel
    {
			public string Email {get;set;}
			[Required]
			public bool EmailConfirmed {get;set;}
			public string PasswordHash {get;set;}
			public string SecurityStamp {get;set;}
			public string PhoneNumber {get;set;}
			[Required]
			public bool PhoneNumberConfirmed {get;set;}
			[Required]
			public bool TwoFactorEnabled {get;set;}
			public DateTime? LockoutEndDateUtc {get;set;}
			[Required]
			public bool LockoutEnabled {get;set;}
			[Required]
			public int AccessFailedCount {get;set;}
			[Required]
			public string UserName {get;set;}
			[Required]
			public string Discriminator {get;set;}
    }
}
