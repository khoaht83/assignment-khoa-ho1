using System;
using System.ComponentModel.DataAnnotations;
using Fw.Core.ViewModel;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// CustomerPassport View Model
    /// Created by KhoaHT
	/// Created Date: Friday, April 29, 2016
    /// </summary>
    public class CustomerPassportViewModel : BaseViewModel
    {
			[Required]
			public string CustomerId {get;set;}
			[Required]
			public Guid BeerCategoryId {get;set;}
    }
}
