using System;
using System.ComponentModel.DataAnnotations;
using Fw.Core.ViewModel;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// Country View Model
    /// Created by KhoaHT
	/// Created Date: Friday, April 29, 2016
    /// </summary>
    public class CountryViewModel : BaseViewModel
    {
			[Required]
			public string Name {get;set;}

    }
}
