﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fw.Core;

namespace Ce.Infrastructure.Domain
{
    public class Country : Entity
    {
        public string Name { get; set; }
        public virtual ICollection<Manufacturer> Manufacturers { get; set; }
    }
}
