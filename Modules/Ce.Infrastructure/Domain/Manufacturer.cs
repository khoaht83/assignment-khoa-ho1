﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fw.Core;

namespace Ce.Infrastructure.Domain
{
    public class Manufacturer : Entity
    {
        public string Name { get; set; }
        public Guid CountryId { get; set; }

        [ForeignKey("CountryId")]
        public Country Country { get; set; }
        
    }
}
