﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fw.Core;

namespace Ce.Infrastructure.Domain
{
    public class BeerCategory : Entity
    {
        public string Name { get; set; }
        public virtual ICollection<Beer> Beers { get; set; }

        public virtual ICollection<CustomerPassport> CustomerPassports { get; set; }
    }
}
